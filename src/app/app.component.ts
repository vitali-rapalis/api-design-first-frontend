import { Component } from '@angular/core';
import { User, UserService } from './core/modules/openapi';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent {
  users$ = this.userService.getUsers();
  user$?: Observable<User>;

  constructor(protected userService: UserService) {}

  findUser(id: string) {
    if (id) {
      this.user$ = this.userService.getUserById(id);
    }
  }
}
