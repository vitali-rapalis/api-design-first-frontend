import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { appRoutes } from './app.routes';
import { ApiModule, BASE_PATH } from './core/modules/openapi';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    ApiModule,
    HttpClientModule,
  ],
  providers: [{ provide: BASE_PATH, useValue: 'http://localhost:9090' }],
  bootstrap: [AppComponent],
})
export class AppModule {}
