# Api Design First Frontend

Adopting an API design-first approach for your frontend project involves designing the API before you start implementing
the actual frontend code.

---

> This project should demonstrate the api design first approach.
> For this frontend project the api interfaces and dto models are generated
> via open-generator [npm package](https://www.npmjs.com/package/@openapitools/openapi-generator-cli) from openapi file,
> the file is public accessible
> over this [url](https://vitali-rapalis.gitlab.io/api-design-first-spec/openapi.yml). The generated code will be a part
> of the source of the project.

## How to build project

For this project npm build tool is used. In package.json postinall script is usesd
to generate code from openapi file with open-generator npm package.

- Install

```
npm i
```

- Run

```
nx serve
```

## How to generate mock server

For this project we will be using [Prism](https://github.com/stoplightio/prism). This is a set of packages for API
mocking and contract testing with OpenAPI v2 (formerly known as Swagger) and OpenAPI v3.x.

- How to create api mocking server

```
prism mock https://vitali-rapalis.gitlab.io/api-design-first-spec/openapi.yml -p 9090
```
